var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var mongoose = require("mongoose");
var Users = require('./api/models/users');
var bodyParser = require("body-parser");
var routes = require('./api/routes/routes.js');
var usersFile = require('./MOCK_DATA.json');
var cors = require('cors');


//mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/bancodb');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
routes(app);//se enrutan las peticiones segùn definiciòn en archivo ./api/routes/routes.js


app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' estas accediendo a una pàgina no disponible :)'})
});
app.listen(port);

console.log('ColApi escuchando en puerto: ' + port + "...");
