var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var users_file = require('./MOCK_DATA.json');
app.listen(port);
console.log('ColApi escuchando en puerto: ' + port + "...");
 
app.get('/colapi/v1/users', function (req, res) {

	res.sendfile('./MOCK_DATA.json');
});

app.put('/colapi/v1', function (req, res) {
	res.send({"msg":"Peticiòn put correct"});
	console.log(req.headers);
	console.log(req.params);
});
app.post('/colapi/v1', function (req, res) {
	res.send({"msg":"Peticiòn post  correct"});
	console.log(req.headers);
	console.log(req.params);
});

app.get('/colapi/v1/users/:id', function (req, res) {
	var pos = req.params.id;
	res.send(users_file[pos -1]);
});
