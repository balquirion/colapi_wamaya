
'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.js');
	var accounts = require('../controllers/accounts.js');
	var movements = require('../controllers/movements.js');
	var baseUrl = "/colapi/v1";//nombre de proyecto y versiòn
	//enrutamiento de users
	app.route(baseUrl + '/users')
		.get(users.list_all_users)
		.post(users.create_a_User);
//enrutamiento de users/id
	app.route(baseUrl + '/users/:id')
		.get(users.read_a_User)
		.put(users.update_a_User)
		.delete(users.delete_a_User);

//enrutamiento accounts
		app.route(baseUrl + '/users/:id/accounts')
			.get(accounts.list_all_accounts)
			.post(accounts.create_a_Account);
//enrutamiento de movimientos
		app.route(baseUrl + '/users/:id/accounts/:iban/movements')
			.get(movements.list_all_movements)
			.post(movements.create_a_Movement);

//enrutamiento de accounts/id
	app.route(baseUrl + '/users/:id')
		.get(accounts.read_a_Account)
		.put(accounts.update_a_Account)
		.delete(accounts.delete_a_Account);

//enrutamiento login Mlab
		app.route(baseUrl + '/login')
			.post(users.login);

//enrutamiento login  Mlab
		app.route(baseUrl + '/logout')
			.post(users.logout);

//enrutamiento login Con Archivo
		app.route(baseUrl + '/loginFile')
			.post(users.loginFile);

//enrutamiento login  Con Archivo
		app.route(baseUrl + '/logoutFile')
			.post(users.logoutFile);
};
