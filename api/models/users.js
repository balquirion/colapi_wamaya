'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  first_name: {
    type: String,
    Required: 'user first_name'
  },
  last_name: {
    type: String,
    Required: 'user last_name'
  },
  email: {
    type: String,
    Required: 'user email'
  },
  password: {
    type: String,
    Required: 'user password'
  }
});


module.exports = mongoose.model('Users', UserSchema);
