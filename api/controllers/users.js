  'use strict';

  var mongoose = require('mongoose');
    var requestJson = require('request-json')
    var MLAB='https://api.mlab.com/api/1/databases/techubank/collections/';
    var apikey = '?apiKey=1kH3J8yFjCXEVQCEDcnMo7YtIxjRbe8w';
    var apikeyQuery = 'apiKey=1kH3J8yFjCXEVQCEDcnMo7YtIxjRbe8w';
    var client = requestJson.createClient(MLAB);
    var usersFile = require('../.././MOCK_DATA.json');

  exports.list_all_users = function(req, resp) {
    client.get('users' + apikey, function(err, res, body) {
      console.log(res.statusCode);
      /*resp.json(res.body);
      resp.send();*/
      resp.send(body);
    });
  };

  exports.create_a_User = function(req, resp) {
    //var user = new User();
    var data = {"first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password};
    client.post(MLAB + '/users' + apikey, data, function(err, res, body) {
      console.log(res.statusCode);
      resp.json(body);
    });
  };

  exports.read_a_User = function(req, resp) {
    var id = req.params.id;
    var queryString='q={id="'+ id+'"}';
    console.log(MLAB + '/users/'+ id + apikey);
    client.get(MLAB + '/users/'+ id + apikey , function(err, res, body) {
      console.log(res.statusCode);
      resp.send(body);
      });
  };

  exports.update_a_User = function(req, resp) {
    var data = {"first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password};
    client.put(MLAB + '/users/'+ req.params.id + apikey, data, function(err, res, body) {
      console.log(res.statusCode);
      resp.json(res);
    });
  };
  // User.remove({}).exec(function(){});
  exports.delete_a_User = function(req, resp) {

    client.delete(MLAB + '/users/'+ req.params.id + apikey , function(err, res, body) {
      console.log(res.statusCode);
      resp.send(body);
    });
  };

  //login
  exports.login = function(req, resp){
    update_User_state(req, resp, true);
  };
  //logout
  exports.logout = function(req, resp){
    update_User_state(req, resp, false);
  };

//Login y logout esta funciòn actualizà el campo de logued con el valor de loginState(true o false)
var update_User_state = function(req, resp, loginState) {

  var email, password;
  if(Object.keys(req.body).length > 0){
     email = req.body.email;
     password = req.body.password;
  }else{
         email = req.query.email;
         password = req.query.password;
      }
      if(email == undefined){
        resp.statusCode=409;
        resp.send({"error":"Debe ingresar un Email"});
      }else {
      var queryString='?q={"email":"'+ email+'"}&';
      //se consulta cliente por correo
      client.get(MLAB + 'users'+ queryString + apikeyQuery , function(err, res, body) {

        var usuario = body[0];
        if(usuario == undefined || usuario.password == undefined){
          resp.statusCode=409;
          resp.send({"error":"credenciales invalidas"})
        }
        //si es un logout o si el password es el almacenado
        else if(!loginState || usuario.password == password){
          usuario.loged = loginState;
          //se actualiza usuario con el estado del login/logout
          client.put(MLAB + '/users/'+ usuario._id.$oid + apikey, usuario, function(err, res, body) {
            resp.send(usuario);
          });
        }else{
          resp.statusCode=409;
          resp.send({"error":"credenciales invalidas"})
        }

      });
    }
};

// Petición login grabando en archivo
exports.loginFile = function(req, resp){
   var email=req.body.email
   var pass=req.body.password

   for(var us of usersFile){
     if(us.email == email){
       if(us.password == pass){
         console.log("pass" + pass);
         us.logged = true;  //añade propiedad logged al usuario
         writeUserDataToFile(usersFile);
         resp.send({"msg" : "login correcto "});
       }
       else{
          resp.send({"msg" : "login incorrecto ", user});
       }
     }
   }
 };

 // Peticiòn logout grabando en archivo
 exports.logoutFile = function(req, resp){
   var email=req.body.email
   for(var us of usersFile){
     if(us.email == email && us.logged){
         us.logged = false;
         writeUserDataToFile(usersFile);
         resp.send({"msg" : "logout correcto "});
     }
     else{
        resp.send({"msg" : "logout no se pudo realizar "});
     }
   }
 };

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./MOCK_DATA.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   })
}
