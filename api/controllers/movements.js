  'use strict';

/**
*Este controlador permite la manipulaciòn de toda la colecciòn movimientos
*para efectos de este proyecto solo se usara el list_all_movements desde el front
*/

    var requestJson = require('request-json')
    var MLAB='https://api.mlab.com/api/1/databases/techubank/collections/';
    var apikey = '?apiKey=1kH3J8yFjCXEVQCEDcnMo7YtIxjRbe8w';
    var apikeyQuery = '&apiKey=1kH3J8yFjCXEVQCEDcnMo7YtIxjRbe8w';
    var client = requestJson.createClient(MLAB);

  exports.list_all_movements = function(req, resp) {
    var id = req.params.id;
    var iban = req.params.iban;
    var queryString='q={"IBAN":"'+ iban+'"}';
    console.log('/movements?'+ queryString + apikeyQuery);

    client.get('movements?'+ queryString + apikeyQuery, function(err, res, body) {
      console.log(res.statusCode);
      resp.statusCode=200;
      resp.send(body);
    });
  };

  exports.create_a_Movement = function(req, resp) {
    //var Account = new Account();
    var data = {"first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password};
    client.post(MLAB + '/movements' + apikey, data, function(err, res, body) {
      console.log(res.statusCode);
      resp.send(body);
    });
  };

  exports.read_a_Account = function(req, resp) {
    var id = req.params.id;
    console.log(id);
    var queryString='q={user_id="'+ id+'"}';
    console.log(MLAB + '/movements/'+ id + apikey);
    client.get(MLAB + '/movements/'+ id + apikey , function(err, res, body) {
      console.log(res.statusCode);
      resp.send(body);
      });
  };

  exports.update_a_Account = function(req, resp) {
    var data = {"first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password};
    client.put(MLAB + '/movements/'+ req.params.id + apikey, data, function(err, res, body) {
      console.log(res.statusCode);
      resp.json(res);
    });
  };
  // Account.remove({}).exec(function(){});
  exports.delete_a_Account = function(req, resp) {

    client.delete(MLAB + '/movements/'+ req.params.id + apikey , function(err, res, body) {
      console.log(res.statusCode);
      resp.json(res.body);
    });
  };
