var mocha = require('../server');
var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');

var should = chai.should();

chai.use(chaiHttp)

describe('prueba colombia', () => {
    it('should ', (done) => {
      chai.request('http://www.bbva.com')
        .get('/')
        .end((err, res)=>{
	        res.should.have.status(200)
          done();
        })

    });
});
describe('prueba colapi', () => {
    it('should ', (done) => {
      chai.request('http://localhost:3000/colapi/v1')
        .get('/users')
        .end((err, res)=>{
	        res.should.have.status(200)
          done();
        })

    });
    it('should ', (done) => {
      chai.request('http://localhost:3000/colapi/v1/users')
        .get('/')
        .end((err, res)=>{
	        res.should.have.status(200)
          done();
        })

    });
    it('should ', (done) => {
      chai.request('http://localhost:3000/colapi/v1')
        .get('/users/5b0dcbf20135ed1d71147560/accounts')
        .end((err, res)=>{
	        res.should.have.status(200)
          done();
        })

    });
    it('devuelve un array de usuarios', (done) => {
      chai.request('http://localhost:3000/colapi/v1/users')
        .get('/')
        .end((err, res)=>{
//console.log(res.body);
//si es un arreglo
	        res.body.should.be.a('array');
//si es mayor a 1
		res.body.length.should.be.gte(1);
    //res.body[0].sould.have.property('first_name');
          done();
        })

    });
});
