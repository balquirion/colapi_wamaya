#Imagen base
FROM node

#Directorio en docker hub
WORKDIR /colapi

#Se agrega contenido al contenedor
ADD . /colapi

#Puerto escucha contenedor
EXPOSE 3000

#comando a ejecutar para lanzar la api
CMD ["npm","start"]
